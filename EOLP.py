import cv2
import numpy as np
import pytesseract
import sqlite3


def readdata():
    con = sqlite3.connect('database.db')

    con.row_factory = sqlite3.Row

    cur = con.cursor()
    cur.execute(
        """
            SELECT * 
            FROM osoba
            WHERE rejestracja =\"""" + text + '"'
         )
    osoby = cur.fetchall()
    for osoba in osoby:
        print(osoba['id'], osoba['rejestracja'], osoba['imie'], osoba['nazwisko'], osoba['opis'])
    con.close()

cap = cv2.VideoCapture('cam2.mp4')

count = 0

while cap.isOpened():
    ret, frame = cap.read()

    if ret:
        cv2.imwrite('out/frame{:d}.jpg'.format(count), frame)
        img = cv2.imread('out/frame{:d}.jpg'.format(count), 1)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        plate_cascade = cv2.CascadeClassifier(r'c:\users\jorda\appdata\local\programs\python\python36\lib\site-packages\cv2\data\haarcascade_russian_plate_number.xml')
        plate = plate_cascade.detectMultiScale(img, minNeighbors = 7)

        if len(plate) == 0:
            None
        else:
            x, y, w, h = plate[-1]
            rect = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.imwrite('out/plate/frame{:d}.jpg'.format(count), rect)
            upper_left = (x, y)
            bottom_right = (x + w, y + h)
            rect_img = rect[upper_left[1]: bottom_right[1], upper_left[0]: bottom_right[0]]
            cv2.imwrite('out/plate/rect/frame{:d}.jpg'.format(count), rect_img)

            gr = cv2.cvtColor(rect_img, cv2.COLOR_BGR2GRAY)
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
            topHat = cv2.morphologyEx(gr, cv2.MORPH_TOPHAT, kernel)
            blackHat = cv2.morphologyEx(gr, cv2.MORPH_BLACKHAT, kernel)
            add = cv2.add(gr, topHat)
            subtract = cv2.subtract(add, blackHat)
            blur = cv2.GaussianBlur(subtract, (5, 5), 0)
            ret, threshold = cv2.threshold(blur, 155, 255, cv2.THRESH_BINARY)

            #cv2.imwrite('out/plate/frame{:d}.jpg'.format(count), threshold)
            text = pytesseract.image_to_string(threshold)
            readdata()


        count += 29
        cap.set(1, count)
    else:
        cap.release()
        break
