import sqlite3


con = sqlite3.connect('database.db')

con.row_factory = sqlite3.Row

cur = con.cursor()

cur.executescript('''
    DROP TABLE IF EXISTS osoba;
    CREATE TABLE IF NOT EXISTS osoba (
        id INTEGER PRIMARY KEY ASC,
        rejestracja varchar(250) NOT NULL,
        imie varchar(250) NOT NULL,
        nazwisko varchar(250) NOT NULL,
        opis varchar(250) NOT NULL
    )''')

osoby = (
    (None, 'PO 3JA71', 'Adam', 'Nowak', 'Pan Adam jest bardzo wymagajacym klientem, liczy na fachowa obsługe i wyjasnienie problemu, Po przegladzie samochód musi byc umyty i odkurzony w środku, zawsze pije czarna kawe bez cukru'),
    (None, 'PN 77269', 'Jan', 'Kos', 'Pan Jan jezdzi samochodem flotowym, cena i czas nie maja dla niego znaczenia wazne aby samochod byl sprawny, zawsze w czasie przegladu Pan Jan wyciaga laptopa i pracuje, dobrze byloby udostepnić temu klientowi salke konferencyjna'),
    (None, 'PN 2465A', 'Piotr', 'Kowalski', 'Pan Piotr ma niemile doswiadczenie po wizytach w innym serwisie, cena ma bardzo duze znaczenie, bardzo dba o swoj samochod i zwraca uwage na kazde nawet najmniejsze zarysowanie')
)

cur.executemany('INSERT INTO osoba VALUES(?,?,?,?,?)', osoby)

con.commit()

def czytajdane():
    cur.execute(
        """
            SELECT osoba.id, rejestracja, imie, nazwisko, opis 
            FROM osoba
        
        """)
    osoby = cur.fetchall()
    for osoba in osoby:
        print(osoba['id'], osoba['rejestracja'], osoba['imie'], osoba['nazwisko'], osoba['opis'])


czytajdane()
con.close()