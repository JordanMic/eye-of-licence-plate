# Eye of licence plate
Projekt przygotowany w ramach projektu studenckiego. Celem projektu było napisanie dowolnej aplikacji w dowolnym języku przy użyciu biblioteki opencv.

## Pomysł na projekt
Pomysł na projekt zaczerpnąłem z mojej ówczesnej pracy jako doradca serwisowy w serwisie samochodowym. Jako doradcy serwisowi mieliśmy kontakt z kilkunastoma klientami dziennie, a co za tym idzie, każdy z tych klientów miał inne wymagania, oczekiwania co do obsługi i inne problemy z autem. Co gdyby doradca zanim jego klient podejdzie do stanowiska dostawał informacje o kliencie typu: jaką kawę pije, ile łyżeczek cukru słodzi, jaką gazetę lubi czytać, czy ostatnio był czymś zainteresowany w salonie itp. Dzięki temu, doradca mógłby zaskoczyć klienta swoim przygotowaniem co mogło by wpłynąć na dobre wrażenie klienta czy nawet poprawe jego humoru.

## Cel projektu
Aplikacja na podstawie nagrania na żywo z kamer np warsztatu samochodowego. Rozpoznaje tablice rejestracyjne i w celu optymalizacji robi im zdjęcia (tylko tablica bez samochodu) i w dalszym kroku rozpoznaje numery rejestracyjne, szuka numerów w bazie i wyświetla przypisane do niego informacje.
